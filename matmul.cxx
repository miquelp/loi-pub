/* 
 * Rectangular matrix multiplication.
 *
 * See the paper ``Cache-Oblivious Algorithms'', by
 * Matteo Frigo, Charles E. Leiserson, Harald Prokop, and 
 * Sridhar Ramachandran, FOCS 1999, for an explanation of
 * why this algorithm is good for caches.
 *
 * Author: Matteo Frigo
 */
static const char *ident __attribute__((__unused__))
     = "$HeadURL: https://bradley.csail.mit.edu/svn/repos/cilk/5.4.3/examples/matmul.cilk $ $LastChangedBy: sukhaj $ $Rev: 517 $ $Date: 2003-10-27 10:05:37 -0500 (Mon, 27 Oct 2003) $";

/* LoI port -- Miquel Pericas, November 2013 
 * There are basically two changes:
 *  1. Exchanged Cilk constructs with spawn/sync macros (see "thread.h")
 *  2. Added LoI instrumentation  */

/*
 * Copyright (c) 2003 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

//#include <cilk-lib.cilkh>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#ifdef MTHREAD
#include "myth.h"
#include <pthread.h>
#endif

#ifdef TBB
#include "tbb/task_scheduler_init.h"
#include "tbb/tbb.h"
#endif


#include "thread.h"

// LoI Instrumentation Setup

#ifdef DO_LOI
#include "loi.h"

#define MATMULKERNEL 0
#define MATMULPHASE  0

struct loi_kernel_info mm_kernels = {
        1,              // 1 kernels in total
        1,              // 1 phases
        {"MatMul_Core"}, 		// kernel names
        {"Matrix Multiplication"}, 	// phase names
        {(1<<MATMULKERNEL)}, 		// kernels/phase
        };

#endif


#define REAL float


void zero(REAL *A, int n)
{
     int i, j;
     
     for (i = 0; i < n; i++) {
	  for (j = 0; j < n; j++) {
	       A[i * n + j] = 0.0;
	  }
     }
}

void m_init(REAL *A, int n)
{
     int i, j;
     
     for (i = 0; i < n; i++) {
	  for (j = 0; j < n; j++) {
	       A[i * n + j] = (double)drand48();
	  }
     }
}

double maxerror(REAL *A, REAL *B, int n)
{
     int i, j;
     double error = 0.0;
     
     for (i = 0; i < n; i++) {
	  for (j = 0; j < n; j++) {
	       double diff = (A[i * n + j] - B[i * n + j]) / A[i * n + j];
	       if (diff < 0)
		    diff = -diff;
	       if (diff > error)
		    error = diff;
	  }
     }
     return error;
}

void iter_matmul(REAL *A, REAL *B, REAL *C, int n)
{
     int i, j, k;
     
     for (i = 0; i < n; i++)
	  for (k = 0; k < n; k++) {
	       REAL c = 0.0;
	       for (j = 0; j < n; j++)
		    c += A[i * n + j] * B[j * n + k];
	       C[i * n + k] = c;
	  }
}

/*
 * A \in M(m, n)
 * B \in M(n, p)
 * C \in M(m, p)
 */
void rec_matmul(REAL *A, REAL *B, REAL *C, int m, int n, int p, int ld,
		     int add)
{
     if ((m + n + p) <= 64) {
#if DO_LOI
     kernel_profile_start();
#endif
	  int i, j, k;
	  /* base case */
	  if (add) {
	       for (i = 0; i < m; i++)
		    for (k = 0; k < p; k++) {
			 REAL c = 0.0;
			 for (j = 0; j < n; j++)
			      c += A[i * ld + j] * B[j * ld + k];
			 C[i * ld + k] += c;
		    }
	  } else {
	       for (i = 0; i < m; i++)
		    for (k = 0; k < p; k++) {
			 REAL c = 0.0;
			 for (j = 0; j < n; j++)
			      c += A[i * ld + j] * B[j * ld + k];
			 C[i * ld + k] = c;
		    }
	  }

#if DO_LOI
  kernel_profile_stop(MATMULKERNEL);
#endif

     } else if (m >= n && n >= p) {
	  int m1 = m >> 1;
          spawn_tasks;
	  spawn_task0(
            rec_matmul(A, B, C, m1, n, p, ld, add));
	  spawn_task0(
            rec_matmul(A + m1 * ld, B, C + m1 * ld, m - m1,
			   n, p, ld, add));
          sync_tasks;
     } else if (n >= m && n >= p) {
	  int n1 = n >> 1;
          spawn_tasks;
	  spawn_task0(
 	    rec_matmul(A, B, C, m, n1, p, ld, add));
	  sync_tasks;
	  spawn_task0(
    	    rec_matmul(A + n1, B + n1 * ld, C, m, n - n1, p, ld, 1));
	  sync_tasks;
     } else {
	  int p1 = p >> 1;
          spawn_tasks;
	  spawn_task0(
	    rec_matmul(A, B, C, m, n, p1, ld, add));
	  spawn_task0(
	    rec_matmul(A, B + p1, C + p1, m, n, p - p1, ld, add));
          sync_tasks;
     }
}

int main(int argc, char *argv[])
{
     int n;
     REAL *A, *B, *C1, *C2;
     int maxthr;

     if (argc != 2) {
	  fprintf(stderr, "Usage: matmul [<cilk options>] <n>\n");
	  exit(1);
	}

#if MTHREAD
     myth_init();
#elif TBB
     task_scheduler_init init(atoi(getenv("TBB_NTHREADS")));
#endif

#if DO_LOI
// Need to manually obtain the total number of threads
#if MTHREAD
    maxthr = myth_get_num_workers();
#elif TBB
    maxthr = atoi(getenv("TBB_NTHREADS"));
#else
    maxthr = 1;
#endif
  loi_init();
 // printf("TSC frequency has been measured to be: %g Hz\n", (double) TSCFREQ);
#if DO_BENCH
  loi_bench();
#endif
#endif




     n = atoi(argv[1]);

     A = (REAL*) malloc(n * n * sizeof(REAL));
     B = (REAL*) malloc(n * n * sizeof(REAL));
     C1 = (REAL*) malloc(n * n * sizeof(REAL));
     C2 = (REAL*) malloc(n * n * sizeof(REAL));
	  
     m_init(A, n);
     m_init(B, n);
     zero(C1, n);
     zero(C2, n);
//     miquel -- deactivated the iterative version as it is too slow
//     iter_matmul(A, B, C1, n);

#if DO_LOI
     phase_profile_start();
#endif
     spawn_tasks;
     spawn_task0(rec_matmul(A, B, C2, n, n, n, n, 0)); 
     sync_tasks;

#if DO_LOI
     phase_profile_stop(MATMULPHASE);
#endif


     	printf("\nCilk Example: matmul\n");
    // printf("Max error     = %g\n", err);
     	printf("Options: size = %d\n", n);
#if DO_LOI
	phase_print_runtime(MATMULPHASE);
#endif

#ifdef DO_LOI 
     printf("number of threads is %d\n", maxthr);
     loi_statistics(&mm_kernels, maxthr);
#endif

     free(C2);
     free(C1);
     free(B);
     free(A);
     return 0;
}
