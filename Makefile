.SUFFIXES: .cxx .cu .o

DEVICE	= CPU
LOI=TIMING

CXX = g++ -g3 -ggdb  -march=native -O3  -funroll-loops -fpermissive -Wno-unused-result -Wall -Wextra # GCC

### Base flags
ifeq ($(LOI), TIMING)
LFLAGS  += -DDO_LOI -DLOI_TIMING -lrt -lm  # use timers -DPROCFREQ=2800000000 
LFLAGS  += -DTIMESTATS  # controls whether the detailed kernel timing statistics are printed
endif
# RDTSCP is only available in newer processor models. check /proc/cpuinfo to detect
# If not available we use a combination of RDTSC and pthread_self()  (linux-only, not portable)
LFLAGS  += -DRDTSCP_ID # Use the RDTSCP method with loi_gettime_id() to get a timestamp and id at the same time
LFLAGS  += -DTIMEBENCH  # Loop at initialization to find out overheads of timers and threadid()

### Debugging flags
LFLAGS	+= -DASSERT # Turns on asserttions (otherwise define an empty macro function)

### Intel TBB flags : TBB is available from http://threadingbuildingblocks.org/download.php
LFLAGS  += -std=c++0x -DTBB -ltbb -L${HOME}/local/lib64/inteltbb -I${HOME}/local/include/inteltbb

### MassiveThreads flags : MassiveThreads is available from http://code.google.com/p/massivethreads/
#LFLAGS += -std=c++0x -DMTHREAD -lmyth-native -DMTH_NATIVE -I${HOME}/local/include -L${HOME}/local/lib64 -ldl

ifdef LOI
LOISOURCES	=  loi.c 
endif

MATSOURCES = matmul.cxx ${LOISOURCES}
SORTSOURCES = sort.cxx getoptions.c ${LOISOURCES}


OBJECTS	= $(SOURCES:.cxx=.o)

#.cxx.o  :
#	$(CXX) -c $^ -o $@ $(LFLAGS)

matmul: ${MATSOURCES}
	${CXX} ${MATSOURCES} -I.  -o matmul ${LFLAGS}

sort: ${SORTSOURCES} 
	${CXX} ${SORTSOURCES} -I.  -o sort ${LFLAGS}

test: matmul sort
	./matmul 512
	./sort 

clean: 
	rm -f matmul sort


