/* loi.c -- Low overhead Instrumentation  
 * See loi.h for additional details */

#include "loi.h"
#include <stdio.h>
#include <math.h>

// counters for kernels and phases
struct loi_kernel_stats kernelstats [MAXTHREADS] __attribute__((aligned(64)));
struct loi_phase_stats phasestats  __attribute__((aligned(64)));

// TSC measured frequency as an integer value
uint64_t TSCFREQ;

// function to measure the frequency of the TSC
static uint64_t vt_pform_wtime(); // from VampirTrace

/* wall-clock time */
// used for measuring TSC clock
static uint64_t __attribute__((unused)) vt_pform_wtime()
{
  uint64_t clock_value;
    /* ... TSC */
    {
      uint32_t low = 0;
      uint32_t high = 0;

      asm volatile ("rdtsc" : "=a" (low), "=d" (high));

      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
    }
  return clock_value;
}

// measure the TSC frequency. This is called from the loi_init() function

static uint64_t __attribute__((unused)) cycle_counter_frequency(long); // from VampirTrace
static uint64_t __attribute__((unused)) cycle_counter_frequency(long nsleep_time)
{
    uint64_t start1_cycle_counter, start2_cycle_counter;
    uint64_t end1_cycle_counter, end2_cycle_counter;
    uint64_t start_time, end_time;
    uint64_t start_time_cycle_counter, end_time_cycle_counter;
    struct timeval timestamp;
    struct timespec nsleeptime = { 0, nsleep_time};

    if(!nsleep_time) nsleep_time = 1e8;

    /* start timestamp */
    start1_cycle_counter = vt_pform_wtime();
    gettimeofday(&timestamp,NULL);
    start2_cycle_counter = vt_pform_wtime();

    start_time=timestamp.tv_sec*1000000+timestamp.tv_usec;

    nanosleep( &nsleeptime, NULL );

    /* end timestamp */
    end1_cycle_counter = vt_pform_wtime();
    gettimeofday(&timestamp,NULL);
    end2_cycle_counter = vt_pform_wtime();

    end_time=timestamp.tv_sec*1000000+timestamp.tv_usec;

    start_time_cycle_counter = (start1_cycle_counter+start2_cycle_counter)/2;
    end_time_cycle_counter   = (  end1_cycle_counter+  end2_cycle_counter)/2;

    /* freq is 1e6 * cycle_counter_time_diff/gettimeofday_time_diff */
    return (uint64_t)
             (1e6*(double)(end_time_cycle_counter-start_time_cycle_counter)/
             (double)(end_time-start_time));
}


// initialize LoI

int loi_init(void)
{
   TSCFREQ = cycle_counter_frequency(5e8);
   return 0; 
}

// Generate statistics 
int loi_statistics(struct loi_kernel_info *kinfo, int total_threads)
{
    double *ktimes; 
    double pertask_numerator = 0.0;
    double pertask_avg;
    double numerator = 0.0, stddev;
    int numthr = 0;
    int kndx = 0; // kernel index
    int ph, bitndx;

    ktimes = (double *) malloc (sizeof(double) * kinfo->num_kernels);

    for(; kndx < kinfo->num_kernels ; kndx++) ktimes[kndx] = 0.0;

// First print kernel statistics
    printf("\n\n**** Kernel Statistics **** \n\n");
    for(kndx = 0; kndx < kinfo->num_kernels; kndx++)
{   
    int i;
    pertask_numerator = 0.0;
    numerator = 0.0;
    numthr=0;
    
    for(i = 0; i < MAXTHREADS; i++)
      if(CTR(Ktime(kndx,i)) > 0.0) 
	{ numthr++;      					// add new kernel
	  ktimes[kndx] += CTR(Ktime(kndx,i)); 			// add kernel time 
	  printf("Kernel %s Thread %d : Total kernels %ld Total Time (s) %g Average Time (ns) %g\n",
	  kinfo->kname[kndx], i, Knum(kndx,i) , CTR(Ktime(kndx,i)), 1e9 * CTR(Ktime(kndx,i)) / Knum(kndx,i));
          pertask_numerator += CTR(Ktime(kndx,i)) / Knum(kndx,i);
	} 
    pertask_avg = pertask_numerator / numthr; // this average is across the number of threads, not number of kernels

    // at this point we have the mean (time/numthr), so we can compute the standard deviation
    for(i = 0; i < MAXTHREADS; i++)
      if(CTR(Ktime(kndx,i)) > 0.0) 
	 numerator += (1e9*CTR(Ktime(kndx,i))/Knum(kndx,i) - (1e9*pertask_avg))*(1e9*CTR(Ktime(kndx,i))/Knum(kndx,i) - (1e9*pertask_avg)); 

    // now the upper part has been computed, so we have to divide it and take the square root  
    stddev = sqrt(numerator/numthr);
    printf("Kernel %s Average %g ns, Standard Deviation %g ns, Coefficient of Variation %g %% \n", kinfo->kname[kndx],1e9*pertask_avg,stddev ,100*stddev / (1e9*pertask_avg));
    printf("Total Kernel %s time %g\n", kinfo->kname[kndx], ktimes[kndx]);
    if(numthr != total_threads) 
     printf("Detected only %d threads\n", numthr);

    printf("\n");
}

// Next print phase statistics

  printf("\n\n**** Phase Statistics **** \n\n");
  for(ph = 0; ph < kinfo->num_phases; ph++)
{
    double accum_kernels = 0.0;
    double overhead = 0.0;
    for(bitndx = 0; bitndx < 63; bitndx++) 
      if(kinfo->phase_kernels[ph] & (((uint64_t)1)<<bitndx)) 
         accum_kernels += ktimes[bitndx];

    printf("%s Execution Time (sec): %g\n", kinfo->phases[ph], CTR(phasestats.exectime[ph]));
    printf("%s Overheads (sec): %g \n", kinfo->phases[ph], CTR(total_threads*phasestats.exectime[ph]) - accum_kernels);
    overhead = (CTR(total_threads*phasestats.exectime[ph]) - accum_kernels)/(CTR(total_threads*phasestats.exectime[ph]));
    printf("%s Relative Overheads (%%): %g  \n",  kinfo->phases[ph], 100*overhead);
    printf("%s Relative Inflation: %g  \n",  kinfo->phases[ph], 1 / (1-overhead));
    printf("\n");
}
    printf("\n");
    return 0;
}


void loi_bench(void)
{
// Benchmark recording of statistics and then clear
  
  ctimer_t tstamp, tstart, tstop;
  uint32_t coreid, ndx;
  tstart = loi_gettime_id(&coreid);
  int bench_iterations = 1e5;
  int i;

  // Benchmark recording of trace and then reinitialize
  for(i = 0; i < bench_iterations; i++){

    tstamp = loi_gettime();
    // do nothing
    tstamp = loi_gettime_id(&coreid);
    ndx = thread_index(coreid);

    Ktime(0,ndx) += tstamp - tstart; // bogus time computation
    Knum(0,ndx) = Knum(0,ndx) + 1;
    }

  tstop = loi_gettime();
  printf("Total time for timing one kernel is %g nsec\n", 1e9*CTR((tstop-tstart)/bench_iterations));

  Ktime(0,ndx) = 0.0; // reset to original value
  Knum(0,ndx) = 0;

}
