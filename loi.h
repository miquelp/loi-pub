/*  loi.h -- main header file */

/* LoI -- Low overhead Instrumentation
 * LoI is a minimalistic library for user level profiling of parallel shared memory applications
 * based on a kernel-centric approach. This library is tied to x86_64 functionality and is thus not portable
 */

#pragma once
#define LOI


#include <sys/time.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

// Needed for syscalls. This however many not be necessary
#include <sys/syscall.h>

// Some constants
// Change this if you need more

// Maximum number of trackable threads 
#define MAXTHREADS 1024

// Maximum Number of kernels and phases
#define NUM_KERNELS  40    
#define NUM_PHASES   20

// TSC cycle timer type
typedef uint64_t  ctimer_t;

/* Definition of the interface */

// Relationship between kernels and phases with string identifiers
// Check the examples
struct loi_kernel_info{
	int num_kernels;  // total number of kernels
	int num_phases;   // total number of phsaes
	const char *kname[NUM_KERNELS]; // kernel names
	const char *phases[NUM_PHASES]; // phase names
	uint64_t phase_kernels[NUM_PHASES];  // bitmask that identifies the kernels within a phase 
};

// one structure entry is only manipulated by a single thread
struct loi_kernel_stats{
  ctimer_t cycles[NUM_KERNELS] __attribute__ ((aligned (64)));
  uint64_t num[NUM_KERNELS] __attribute__((packed)); 
};

struct loi_phase_stats{
   ctimer_t exectime[NUM_PHASES] __attribute__ ((aligned (64)));
};

// This structure holds all the bookkeeping
extern struct loi_kernel_stats kernelstats [MAXTHREADS] __attribute__((aligned(64)));
extern struct loi_phase_stats phasestats __attribute__((aligned(64)));

/* Initialization */
int loi_init(void);

// holds the speed of the local TSC. Set by loi_init()
extern uint64_t TSCFREQ;

// Macro to access kernel timers and counters
//   Uses: Kernel #id, kernel stats pointer, thread index
#define   Ktime(kid,tid)  (kernelstats[tid].cycles[kid])
#define   Knum(kid,tid)   (kernelstats[tid].num[kid])

// functions to access timestamp counters

// RDTSC
static inline ctimer_t __attribute__((unused)) rdtsc_clock_gettime(void)
{
      uint64_t clock_value;
      uint32_t low = 0;
      uint32_t high = 0;
      asm volatile ("rdtsc" : "=a" (low), "=d" (high));
      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
      return (ctimer_t) clock_value;
}

// RDTSCP 
static inline ctimer_t __attribute__((unused)) rdtsc_clock_gettime_serial(void)
{
      uint64_t clock_value;
      uint32_t low = 0;
      uint32_t high = 0;
      uint32_t aux;
      asm volatile ("rdtscp" : "=a" (low), "=d" (high), "=c" (aux)); // we ignore the result in aux
      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
      return (ctimer_t) clock_value;
}

// RDTSCP with processor id
static inline ctimer_t __attribute__((unused)) rdtsc_clock_gettime_serial_id(uint32_t  *pid)
{
      uint64_t clock_value;
      uint32_t low = 0;
      uint32_t high = 0;
      if (!pid) return 0;
      asm volatile ("rdtscp" : "=a" (low), "=d" (high), "=c" (*pid)); 
      clock_value = ((uint64_t)high << 32) | (uint64_t)low;
      return (ctimer_t) clock_value;
}


// Can choose which timer to use by default

#ifndef TIMER
 #define TIMER 1
#endif

#if TIMER == 1
#define   loi_gettime(id)    rdtsc_clock_gettime()            // RDTSC method
#elif TIMER == 2
#define   loi_gettime(id)    rdtsc_clock_gettime_serial()     // RDTSCP method
#else 
  #error "Invalid timer specification"
#endif


// We can also choose a method for obtaining a thread id

// You may need to tune the value of MAXTHREADS depending on the selected method
// Particularly pthreads is dangerous, as a pthread_t should not be used as a numerical value. 
// However, in the case of NPTL the value follows a certain pattern which makes it "usable"

#ifndef THREADID
#define THREADID 2
#endif

#if THREADID == 1
#define threadid()  syscall(SYS_gettid)    // system call method, too much overhead
#elif THREADID == 2
#include <pthread.h>
#define threadid() pthread_self()   // works but is outside POSIX
#elif THREADID == 3
#define threadid() ((0))        // to reduce overhead when only one thread is running
#else
#error "Invalid THREADID"
#endif

// If the processor supports the RDTSCP instruction (check /proc/cpuinfo flags) then a very 
// efficient way to retrieve the index is to use the IA32_TSC_AUX register value
#if RDTSCP_ID
#define thread_index(coreid) ((coreid & 0xfff) )		// lower 12 bits is core id
#define  loi_gettime_id(a)    rdtsc_clock_gettime_serial_id(a)  // RDTSCP method
#else
#define  thread_index(coreid) (threadid() %MAXTHREADS)	        // otherwise ensure offsets by using modulo
#define  loi_gettime_id(a)    loi_gettime(a)                    // if not use the method specified by TIMER
#endif

// tranlate from cycle count to real time (CTR)
// a should be a relative time count, otherwise you have no precision in this operation
#define CTR(a) ((double) a/ (double) TSCFREQ)

// Print statistics
int loi_statistics(struct loi_kernel_info *, int total_threads); 

void loi_bench(void);

// Finally here is an attempt to provide macros for profiling
// Again, check the examples as a tutorial on how to use this
// Note that these macros define a code blocks, which has some implications on the scope of variables

// this is necessary for OMP profiling
#define PRAGMA(x)                       _Pragma( #x )
 
#define phase_profile_start()           { ctimer_t __pstart, __pstop; \
                                        __pstart = loi_gettime(); 

// for OpenMP parallel sections only the master should record phase profiles
#define phase_profile_start_omp()       { ctimer_t __pstart, __pstop; \
				        PRAGMA(omp master) \
                                        __pstart = loi_gettime();  \
                                        PRAGMA(omp barrier) 


#define phase_profile_stop(phase)       __pstop = loi_gettime(); \
                                        phasestats.exectime[phase] = __pstop - __pstart; }

#define phase_profile_stop_omp(phase)   PRAGMA(omp master) \
					{ __pstop = loi_gettime(); \
                                        phasestats.exectime[phase] = __pstop - __pstart; }   \
                                        PRAGMA(omp barrier)  }

#define kernel_profile_start()          { uint32_t __coreid; int __ndx; \
                                        ctimer_t __kstart, __kstop __attribute__((unused)); \
                                        __kstart = loi_gettime();

#define kernel_profile_stop(kindex)     __kstop = loi_gettime_id(&__coreid); \
                                        __ndx = thread_index(__coreid); \
                                        Ktime(kindex,__ndx) += __kstop - __kstart; \
                                        Knum(kindex,__ndx) = Knum(kindex,__ndx) + 1; }

#define phase_print_runtime(phase)      printf("Running time for phase %d is %g s\n", phase, CTR(phasestats.exectime[phase]));

